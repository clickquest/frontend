const fs = require('fs');
const packageJson = require('./package.json');

const version = {
  version: packageJson.version
};

fs.writeFileSync('./src/version.json', JSON.stringify(version, null, 2));