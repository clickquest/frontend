import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private apiUrl = 'https://api.clickquest.webdad.eu'; // Update this to your backend URL

  constructor(private http: HttpClient) { }

  registerPlayer(name: string, password: string): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/register`, { name, password });
  }

  loginPlayer(name: string, password: string): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/login`, { name, password });
  }

  doHeroStuff(playerId: number, taskId: number): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/players/${playerId}/hero-stuff`, { task_id: taskId });
  }

  getPlayer(playerId: number): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/players/${playerId}`);
  }

  getTasks(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/tasks`);
  }

  getHighscore(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/highscore`);
  }
}