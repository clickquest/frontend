import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../player.service';
import { interval } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { NgFor, NgIf } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-home',
  standalone: true,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  imports: [FormsModule, NgFor, NgIf]
})
export class HomeComponent implements OnInit {
  registerName: string = '';
  registerPassword: string = '';
  loginName: string = '';
  loginPassword: string = '';
  player: any;
  tasks: any[] = [];
  isBusy: boolean = false;
  remainingTime: number = 0;
  highscore: any[] = [];
  isLoggedIn: boolean = false;

  constructor(private playerService: PlayerService, private cookieService: CookieService) {}

  ngOnInit(): void {
    this.loadHighscore();
    this.loadTasks();
    const playerId = this.cookieService.get('playerId');
    if (playerId) {
      this.playerService.getPlayer(+playerId).subscribe(player => {
        this.player = player;
        this.isLoggedIn = true;
      });
    }
    interval(1000).subscribe(() => {
      if (this.isBusy && this.remainingTime > 0) {
        this.remainingTime--;
        if (this.remainingTime <= 0) {
          this.refreshPlayer();
        }
      }
    });
  }

  register(): void {
    this.playerService.registerPlayer(this.registerName, this.registerPassword).subscribe(response => {
      alert('Registration successful');
    });
  }

  login(): void {
    this.playerService.loginPlayer(this.loginName, this.loginPassword).subscribe(player => {
      this.player = player;
      this.isLoggedIn = true;
      this.cookieService.set('playerId', player.id);
      this.cookieService.set('username', this.loginName);
    }, error => {
      alert('Invalid credentials');
    });
  }

  doHeroStuff(taskId: number): void {
    this.playerService.doHeroStuff(this.player.id, taskId).subscribe(() => {
      this.refreshPlayer();
    });
  }

  loadTasks(): void {
    this.playerService.getTasks().subscribe(tasks => {
      this.tasks = tasks;
    });
  }

  loadHighscore(): void {
    this.playerService.getHighscore().subscribe(highscore => {
      this.highscore = highscore;
    });
  }

  canDoTask(task: any): boolean {
    const playerLevel = Math.floor(this.player.xp / 100); // Example level calculation
    return playerLevel >= task.minimum_level;
  }

  refreshPlayer(): void {
    this.playerService.getPlayer(this.player.id).subscribe(player => {
      this.player = player;
      this.isBusy = player.is_busy;
      if (this.isBusy) {
        this.remainingTime = player.remaining_time;
      }
    });
  }
}