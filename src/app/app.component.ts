import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {MatToolbarModule} from '@angular/material/toolbar';
import { NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MatToolbarModule, NgIf],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  version: string = '';
  loggedInUser: string | null = null;

  constructor(private http: HttpClient, private cookieService: CookieService) {}

  ngOnInit(): void {
    this.fetchVersion();
    this.loggedInUser = this.cookieService.get('username'); // Assuming you store the username in a cookie
  }

  fetchVersion(): void {
    this.http.get<any>('version.json').subscribe(packageJson => {
      this.version = packageJson.version;
    });
  }
}
