import { bootstrapApplication } from '@angular/platform-browser';
import { enableProdMode, importProvidersFrom } from "@angular/core";

import { HttpClientModule } from "@angular/common/http";

import { appConfig } from './app/app.config';
import { routes } from './app/app.routes';

import { AppComponent } from './app/app.component';
import { provideRouter } from '@angular/router';

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(HttpClientModule),
    provideRouter(routes),
  ],
}).catch((err) => console.error(err));
